/*
 * An argument parsing library for Gtk#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Text;

namespace ArgumentParsing
{
    /// <summary>
    /// The parser that parses a single argument
    /// </summary>
	public class Parser
	{
		private enum State {
            /// <summary>
            /// The parser is reading first dash.
            /// </summary>
			ReadingFirstDash,
            /// <summary>
            /// The parser is reading second dash.
            /// </summary>
			ReadingSecondDash,
            /// <summary>
            /// The parser is reading name of the reading.
            /// </summary>
			ReadingName,
            /// <summary>
            /// The parser is reading value.
            /// </summary>
			ReadingValue
		}

		private State state;
		private StringBuilder name;
		private StringBuilder val;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentParsing.Parser"/> class.
        /// </summary>
		public Parser()
		{
			state = State.ReadingFirstDash;
			name = new StringBuilder();
			val = new StringBuilder();
		}

        /// <summary>
        /// Parse the specified argument.
        /// </summary>
        /// <param name="argument">Argument.</param>
		public KeyValuePair<string, string> Parse(string argument)
		{
			foreach(char c in argument) {
				ParseChar(c);
			}
			return new KeyValuePair<string, string>(name.ToString(), val.ToString());
		}

		private void ParseChar(char c)
		{
			switch(state) {
			case State.ReadingFirstDash:
			case State.ReadingSecondDash:
				if (c != '-') {
					throw new ArgumentException("options must start with a double-dash");
				}
				state++;
				break;
			case State.ReadingName:
				if (c == '=') {
					state = State.ReadingValue;
				}
				else
				{
					name.Append(c);
				}
				break;
			case State.ReadingValue:
				val.Append(c);
				break;
			}
		}
	}
}

