/*
 * An argument parsing library for Gtk#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace ArgumentParsing
{
    /// <summary>
    /// Parser for command line arguments
    /// </summary>
	public class ArgumentParser
	{
		private Dictionary<string, string> parsedArguments;
		private List<string> errorArguments;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentParsing.ArgumentParser"/> class.
        /// </summary>
		public ArgumentParser()
		{
			parsedArguments = new Dictionary<string, string>();
			errorArguments = new List<string>();
		}

        /// <summary>
        /// Parse the specified arguments.
        /// </summary>
        /// <param name="args">Arguments.</param>
		public void Parse(string[] args)
		{
			foreach(string arg in args)
			{
				ParseArgument(arg);
			}
		}

        /// <summary>
        /// Parses the argument.
        /// </summary>
        /// <param name="arg">Argument.</param>
		private void ParseArgument(string arg)
		{
			Parser parser = new Parser();
			try {
				KeyValuePair<string, string> option = parser.Parse(arg);
				parsedArguments.Add(option.Key, option.Value);
			}
			catch(Exception)
			{
				errorArguments.Add(arg);
			}
		}

        /// <summary>
        /// Gets the parsed arguments.
        /// </summary>
        /// <value>The parsed arguments.</value>
		public IDictionary<string, string> ParsedArguments
		{
			get {
				return parsedArguments;
			}
		}

        /// <summary>
        /// Gets the error arguments.
        /// </summary>
        /// <value>The error arguments.</value>
		public List<string> ErrorArguments {
			get {
				return errorArguments;
			}
		}

        /// <summary>
        /// Gets a value indicating whether the arguments could not be parsed.
        /// </summary>
        /// <value><c>true</c> if an error occured while parsing the arguments; otherwise, <c>false</c>.</value>
		public bool HasError {
			get {
				return errorArguments.Count > 0;
			}
		}
	}
}

